<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="btn_in" />
        <signal name="tape_in" />
        <signal name="tape_out" />
        <signal name="sound_out" />
        <signal name="clk" />
        <signal name="XLXN_82" />
        <signal name="XLXN_1" />
        <signal name="XLXN_5" />
        <signal name="XLXN_7" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="aud_l" />
        <signal name="aud_r" />
        <signal name="XLXN_6" />
        <signal name="XLXN_27" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_8" />
        <port polarity="Input" name="btn_in" />
        <port polarity="Output" name="tape_in" />
        <port polarity="Input" name="tape_out" />
        <port polarity="Input" name="sound_out" />
        <port polarity="Input" name="clk" />
        <port polarity="BiDirectional" name="aud_l" />
        <port polarity="BiDirectional" name="aud_r" />
        <blockdef name="cb2ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="iobuf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="128" y1="-128" y2="-128" x1="224" />
            <line x2="128" y1="-64" y2="-64" x1="160" />
            <line x2="160" y1="-128" y2="-64" x1="160" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="96" y1="-140" y2="-192" x1="96" />
            <line x2="96" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-96" y2="-160" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="128" />
            <line x2="128" y1="-160" y2="-128" x1="64" />
            <line x2="128" y1="-96" y2="-32" x1="128" />
            <line x2="128" y1="-64" y2="-96" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="and2b1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="40" y1="-64" y2="-64" x1="0" />
            <circle r="12" cx="52" cy="-64" />
        </blockdef>
        <blockdef name="debounce">
            <timestamp>2020-5-3T21:52:23</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="debounce" name="u_deb_btn_in">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="btn_in" name="btn" />
            <blockpin signalname="XLXN_82" name="sig" />
        </block>
        <block symbolname="cb2ce" name="u_cnt2">
            <blockpin signalname="XLXN_82" name="C" />
            <blockpin signalname="XLXN_1" name="CE" />
            <blockpin signalname="XLXN_5" name="CLR" />
            <blockpin name="CEO" />
            <blockpin signalname="XLXN_27" name="Q0" />
            <blockpin signalname="XLXN_6" name="Q1" />
            <blockpin name="TC" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="tape_in" name="O" />
        </block>
        <block symbolname="iobuf" name="XLXI_4">
            <attr value="8" name="DRIVE">
                <trait verilog="all:0 wsynop:1 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="XLXN_11" name="I" />
            <blockpin signalname="aud_l" name="IO" />
            <blockpin signalname="XLXN_8" name="O" />
            <blockpin signalname="XLXN_6" name="T" />
        </block>
        <block symbolname="iobuf" name="XLXI_5">
            <attr value="8" name="DRIVE">
                <trait verilog="all:0 wsynop:1 wsynth:1" />
                <trait vhdl="all:0 wa:1 wd:1" />
            </attr>
            <blockpin signalname="XLXN_11" name="I" />
            <blockpin signalname="aud_r" name="IO" />
            <blockpin signalname="XLXN_7" name="O" />
            <blockpin signalname="XLXN_6" name="T" />
        </block>
        <block symbolname="and2b1" name="XLXI_11">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="sound_out" name="I1" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="tape_out" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_10">
            <blockpin signalname="XLXN_16" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_8">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_2">
            <blockpin signalname="XLXN_1" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="tape_in">
            <wire x2="1600" y1="1696" y2="1696" x1="464" />
            <wire x2="1616" y1="1696" y2="1696" x1="1600" />
        </branch>
        <branch name="tape_out">
            <wire x2="1744" y1="1008" y2="1008" x1="464" />
            <wire x2="1760" y1="1008" y2="1008" x1="1744" />
        </branch>
        <branch name="sound_out">
            <wire x2="1744" y1="880" y2="880" x1="464" />
            <wire x2="1760" y1="880" y2="880" x1="1744" />
        </branch>
        <branch name="clk">
            <wire x2="528" y1="608" y2="608" x1="464" />
        </branch>
        <branch name="btn_in">
            <wire x2="528" y1="672" y2="672" x1="464" />
        </branch>
        <iomarker fontsize="28" x="464" y="672" name="btn_in" orien="R180" />
        <iomarker fontsize="28" x="464" y="608" name="clk" orien="R180" />
        <instance x="528" y="704" name="u_deb_btn_in" orien="R0">
        </instance>
        <instance x="1024" y="1584" name="u_cnt2" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="976" y1="1312" y2="1392" x1="976" />
            <wire x2="1024" y1="1392" y2="1392" x1="976" />
        </branch>
        <instance x="1488" y="1360" name="XLXI_3" orien="R90" />
        <branch name="XLXN_5">
            <wire x2="1024" y1="1552" y2="1632" x1="1024" />
            <wire x2="1584" y1="1632" y2="1632" x1="1024" />
            <wire x2="1584" y1="1616" y2="1632" x1="1584" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="2528" y1="1760" y2="1760" x1="2144" />
        </branch>
        <instance x="1872" y="1600" name="XLXI_9" orien="R180" />
        <branch name="XLXN_9">
            <wire x2="1888" y1="1728" y2="1728" x1="1872" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="2400" y1="976" y2="976" x1="2320" />
            <wire x2="2400" y1="976" y2="1392" x1="2400" />
            <wire x2="2528" y1="1392" y2="1392" x1="2400" />
            <wire x2="2400" y1="1392" y2="1696" x1="2400" />
            <wire x2="2528" y1="1696" y2="1696" x1="2400" />
        </branch>
        <instance x="2528" y="1520" name="XLXI_4" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="DRIVE" x="0" y="-248" type="instance" />
        </instance>
        <instance x="2528" y="1824" name="XLXI_5" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="DRIVE" x="0" y="-248" type="instance" />
        </instance>
        <branch name="aud_l">
            <wire x2="2960" y1="1392" y2="1392" x1="2752" />
        </branch>
        <branch name="aud_r">
            <wire x2="2960" y1="1696" y2="1696" x1="2752" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1552" y1="1328" y2="1328" x1="1408" />
            <wire x2="1552" y1="1328" y2="1360" x1="1552" />
            <wire x2="1888" y1="1328" y2="1328" x1="1552" />
            <wire x2="1888" y1="1328" y2="1664" x1="1888" />
            <wire x2="2464" y1="1328" y2="1328" x1="1888" />
            <wire x2="2528" y1="1328" y2="1328" x1="2464" />
            <wire x2="2464" y1="1328" y2="1632" x1="2464" />
            <wire x2="2528" y1="1632" y2="1632" x1="2464" />
            <wire x2="1888" y1="1664" y2="1664" x1="1872" />
        </branch>
        <instance x="1760" y="1008" name="XLXI_11" orien="R0" />
        <branch name="XLXN_27">
            <wire x2="1616" y1="1264" y2="1264" x1="1408" />
            <wire x2="1616" y1="1264" y2="1360" x1="1616" />
            <wire x2="1760" y1="944" y2="944" x1="1616" />
            <wire x2="1616" y1="944" y2="1072" x1="1616" />
            <wire x2="1616" y1="1072" y2="1264" x1="1616" />
            <wire x2="1760" y1="1072" y2="1072" x1="1616" />
        </branch>
        <instance x="1760" y="1136" name="XLXI_12" orien="R0" />
        <instance x="2064" y="1072" name="XLXI_10" orien="R0" />
        <branch name="XLXN_15">
            <wire x2="2032" y1="912" y2="912" x1="2016" />
            <wire x2="2032" y1="912" y2="944" x1="2032" />
            <wire x2="2064" y1="944" y2="944" x1="2032" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="2032" y1="1040" y2="1040" x1="2016" />
            <wire x2="2064" y1="1008" y2="1008" x1="2032" />
            <wire x2="2032" y1="1008" y2="1040" x1="2032" />
        </branch>
        <instance x="2144" y="1632" name="XLXI_8" orien="R180" />
        <branch name="XLXN_8">
            <wire x2="2320" y1="1696" y2="1696" x1="2144" />
            <wire x2="2320" y1="1456" y2="1696" x1="2320" />
            <wire x2="2528" y1="1456" y2="1456" x1="2320" />
        </branch>
        <instance x="912" y="1312" name="XLXI_2" orien="R0" />
        <iomarker fontsize="28" x="2960" y="1392" name="aud_l" orien="R0" />
        <iomarker fontsize="28" x="2960" y="1696" name="aud_r" orien="R0" />
        <branch name="XLXN_82">
            <wire x2="928" y1="608" y2="608" x1="912" />
            <wire x2="928" y1="608" y2="1456" x1="928" />
            <wire x2="1024" y1="1456" y2="1456" x1="928" />
        </branch>
        <iomarker fontsize="28" x="464" y="880" name="sound_out" orien="R180" />
        <iomarker fontsize="28" x="464" y="1008" name="tape_out" orien="R180" />
        <iomarker fontsize="28" x="464" y="1696" name="tape_in" orien="R180" />
    </sheet>
</drawing>