// VGA 1024 x 768 @ 60 Hz - input frequency 16 MHz
`timescale 1ns / 100ps

///RAM module//////////////////////////////////////////////////////////////////
	//Thanks to Dan Strother for his blog article (on danstrother.com):
	//"Inferring true dual-port, dual-clock RAMs in Xilinx and Altera FPGAs"
module vga_ram #(
	parameter ADDR_WIDTH = 10, 
	parameter DATA_WIDTH = 8,
	parameter MEMORYSIZE = 2**ADDR_WIDTH
	) (
	input wire a_clk, 
	input wire a_wr, 
	input wire [ADDR_WIDTH-1:0] a_addr, 
	input wire [DATA_WIDTH-1:0] a_din, 
	output reg [DATA_WIDTH-1:0] a_dout,
	input wire b_clk, 
	input wire b_wr, 
	input wire [ADDR_WIDTH-1:0] b_addr, 
	input wire [DATA_WIDTH-1:0] b_din, 
	output reg [DATA_WIDTH-1:0] b_dout
	);
	reg [DATA_WIDTH-1:0] mem [MEMORYSIZE-1:0];
	always @(posedge a_clk) begin
		a_dout <= mem[a_addr];
		if (a_wr) begin
			a_dout 		<= a_din;
			mem[a_addr] <= a_din;
		end
	end
	always @(posedge b_clk) begin
		b_dout <= mem[b_addr];
		if (b_wr) begin
			b_dout 		<= b_din;
			mem[b_addr] <= b_din;
		end
	end
endmodule


///VGA module//////////////////////////////////////////////////////////////////
module ace2vga(
    input Rst_n,
    input ace_clk,
	 input vga_clk,
    input WE_n,
    input [15:0] Addr,
    input [7:0] DI,
    output hsync,
    output vsync,
    output reg [3:0] video_r,
    output reg [3:0] video_g,
    output reg [3:0] video_b
    );
	
///VGA generator///////////////////////////////////////////////////////////////
	//Thanks to Tomi Engdahl's "Video Timing Generator" 
	//at http://www.epanorama.net/faq/vga2rgb/calc.html
	//Thanks to Zainalabedin Navabi's "Embedded Core Design with FPGAs"
	//for the "Fig. 7.42 Monitor Synchronization Hardware" sample code.
	//
	//VESA mode 1024 x 768 @ 60Hz
	//pix_clk = 65 MHz
	//max_hor = 1334 = 1024 + 24 + 136 + 160 (content/fp/sync/bp)
	//        => frq_hor = 48,363 kHz  (pix_clk/max_hor)
	//max_ver = 806 = 768 + 3 + 6 + 29 (content/fp/sync/bp)
	//
	//
	//JupiterACE: 256 x 192 pixels => 4 x 4 field @ 1024 x 768
	//=> 256 x 768 @ 60 Hz
	//pix_clk = 65MHz / 4 == 16,250 MHz
	//max_hor = 336 = 256 + 6 + 34 + 40 (content/fp/sync/bp)
	//        => frq_hor = 48,363 kHz  (pix_clk/max_hor)
	//max_ver = 806 = (192 * 4) + 3 + 6 + 29 (content/fp/sync/bp)
	//
	//if pixelclock := 16,000 MHz  (32Mhz div 2)
	//if max_hor = 336 
	//        => frq_hor = 47,619 kHz   (pix_clk/max_hor)
	//        != frq_hor = 48,363 kHz ==> 1,5623% diff.
	//==> correction by adjusting max_hor to achieve frq_hor
	//    frq_hor =!= 48,363 kHz
	//    max_hor := 331 => frq_hor=48,338kHz => delta = -511 ppm 
	//    max_hor := 330 => frq_hor=48,485kHz => delta = 2517 ppm
	//==> shave 5 or 6 pxl off 336 (e.g. from hor_bp or hor_sync)
	//==> max_hor = 331 or 332

	reg [8:0] cnt_hor;
	reg [9:0] cnt_ver;
	wire viden;
	wire video;
	wire curpix;

	// VGA params - active / front_porch / sync / back_porch
	// horizontal -    256 /           6 /   34 /         40  @ 16.250 MHz
	// hor pos    -    256 /         262 /  296 /        336  @ 16.250 MHz
	// hor @ 16MHz-    256 /           5 /   32 /         38  @ 16.000 MHz
	// hor pos    -    256 /         261 /  293 /        331  @ 16.000 MHz
	// vertical   -    768 /           3 /    6 /         29  @ 48.363 kHz
	// ver pos    -    768 /         771 /  777 /        806  @ 48.363 kHz
	//  frequency - 16MHz pix / 48.363kHz hor / 60Hz ver
	
	// on real monitor with auto-adjust, the computed back_porch
	// (both hor and ver) looked way too large
	parameter HSYfm = 280, HSYto = 314, HorMax = 330;
	parameter VSYfm = 771, VSYto = 777, VerMax = 804;
	
	always @(posedge vga_clk) begin
		if (!Rst_n || cnt_hor == HorMax) cnt_hor <= 0; 
		else cnt_hor <= cnt_hor + 1;
	end
	
	always @(posedge vga_clk) begin
		if (!Rst_n || (cnt_ver == VerMax && cnt_hor == HSYto)) cnt_ver <= 0; 
		else if (cnt_hor == HSYto) cnt_ver <= cnt_ver + 1;
	end

	assign hsync = (cnt_hor >= HSYfm && cnt_hor <= HSYto) ? 1'b0 : 1'b1;
	assign vsync = (cnt_ver >= VSYfm && cnt_ver <= VSYto) ? 1'b0 : 1'b1;

	// VGA active (we buffer, if we use cnt_hor < 256, we miss the last pixel)
	assign viden = (cnt_hor < 258 && cnt_ver < 768) ? 1'b1 : 1'b0; 
	assign video = viden & curpix;

	// Video test
	// assign curpix = cnt_ver[5] ^ cnt_hor[4];


///Video and Character RAM shadow (CPU write-only, VGA read-only)//////////////

	wire [7:0] vga_char;
	wire [7:0] vga_cpix;
	wire [9:0] VRAM_addr; // VideoRAM shadow only needs 768 bytes (3 x 256)
	wire [9:0] CRAM_addr;
	wire VRAM_wren;
	wire CRAM_wren;
	wire VidR_wren;

	assign VidR_wren = !WE_n && (Addr[15:12] == 4'b0010);
	assign VRAM_wren = VidR_wren & !Addr[11] & !(Addr[9] & Addr[8]); // 768 bytes only
	assign CRAM_wren = VidR_wren &  Addr[11];
	assign VRAM_addr = { cnt_ver[9:5],  cnt_hor[7:3] }; // hor clock / 8 (char)
	assign CRAM_addr = { vga_char[6:0], cnt_ver[4:2] }; // ver clock / 4

	vga_ram #( .MEMORYSIZE(768) )
	VidRAM (.a_clk(ace_clk),.a_wr(VRAM_wren),.a_addr(Addr[9:0]),.a_din(DI),   .a_dout(),
			.b_clk(vga_clk),.b_wr(1'b0),     .b_addr(VRAM_addr),.b_din(8'b0), .b_dout(vga_char));
	vga_ram 
	ChrRAM (.a_clk(ace_clk),.a_wr(CRAM_wren),.a_addr(Addr[9:0]),.a_din(DI),   .a_dout(),
			.b_clk(vga_clk),.b_wr(1'b0),     .b_addr(CRAM_addr),.b_din(8'b0), .b_dout(vga_cpix));

	// Unbuffered output
	// assign curpix = vga_cpix[7 - cnt_hor[3:1]] ^ vga_char[7];

	reg [7:0] buf_cpix;
	reg [2:0] cnt_cpix;
	reg buf_cinv;

	always @(posedge vga_clk) begin
		if (!Rst_n || !viden) begin
			cnt_cpix = 0;
			buf_cpix <= 8'h00;
			buf_cinv <= 0;
		end else begin 
			if (cnt_cpix == 2) begin 
				buf_cpix <= vga_cpix;
				buf_cinv <= vga_char[7];
			end else begin
				buf_cpix[7:1] <= buf_cpix[6:0];
			end
			cnt_cpix = cnt_cpix + 1;
		end
	end

	assign curpix = buf_cpix[7] ^ buf_cinv;


///Papilio One Video output multiplexer////////////////////////////////////////

	always @(video)
	begin
		video_r[3:0] <= { video, video, video, video };
		video_g[3:0] <= { video, video, video, video };
		video_b[3:0] <= { video, video, video, video };
	end

endmodule




