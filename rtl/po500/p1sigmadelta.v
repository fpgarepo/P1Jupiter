// RIPPED FROM GAMEDUINO

// This is a Delta-Sigma Digital to Analog Converter
// MSBI = Most significant Bit of DAC input, 12 means 13-bit

module dac #( 
	parameter MSBI = 12 
	)(
	output reg		DACout, 	// This is the average output that feeds low pass filter, 
									// for optimum performance, ensure that this ff is in IOB
	input [MSBI:0]	DACin,	// DAC input (excess 2**MSBI)
	input				Clk,
	input				Reset
	);
	reg [MSBI+2:0] DeltaAdder; // Output of Delta adder
	reg [MSBI+2:0] SigmaAdder; // Output of Sigma adder
	reg [MSBI+2:0] SigmaLatch; // Latches output of Sigma adder
	reg [MSBI+2:0] DeltaB;	// B input of Delta adder

//	always @(SigmaLatch) DeltaB = {SigmaLatch[MSBI+2], SigmaLatch[MSBI+2]} << (MSBI+1);
//	always @(DACin or DeltaB) DeltaAdder = DACin + DeltaB;
//	always @(DeltaAdder or SigmaLatch) SigmaAdder = DeltaAdder + SigmaLatch;
	always @(posedge Clk)
	begin
	  if (Reset) begin
		 SigmaLatch = 1'b1 << (MSBI+1);
		 DACout = 1'b0;
	  end else begin
		 SigmaLatch = SigmaAdder;
		 DeltaB = {SigmaLatch[MSBI+2], SigmaLatch[MSBI+2]} << (MSBI+1);
		 DeltaAdder = DACin + DeltaB;
		 SigmaAdder = DeltaAdder + SigmaLatch;
		 DACout = SigmaLatch[MSBI+2];
	  end
	end
endmodule // dac

module sigmadelta(
  input	clk,
  input	left,
  input	right,
  output	AUDIOL,
  output	AUDIOR
  );
  
  parameter MSBI = 12;

  dac lxdac(.DACout(AUDIOL), .DACin({MSBI+1{left }}), .Clk(clk), .Reset(1'b0));
  dac rxdac(.DACout(AUDIOR), .DACin({MSBI+1{right}}), .Clk(clk), .Reset(1'b0));
endmodule // sigmadelta
