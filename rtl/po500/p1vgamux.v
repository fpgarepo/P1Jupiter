`timescale 1ns / 1ps
//PapilioOne VGA
module p1vga(
    input vid,
    output reg [3:0] video_r,
    output reg [3:0] video_g,
    output reg [3:0] video_b
	);

	always @(vid)
	begin
		video_r[3:0] <= { vid, 3'b000 };
		video_g[3:0] <= { vid, 3'b000 };
		video_b[3:0] <= { vid, 3'b000 };
	end
endmodule
