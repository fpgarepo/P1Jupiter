// P1Jupiter clocks
`timescale 1ns / 1ps

module p1clock(input CLKIN, RST_IN, output CLK6M5, LOCKED, CLK32IB, CLK16M);

	wire CLKIN_IBUFG;
	wire CLKFB_IN;
	wire CLK2X_BUF;
	wire CLK2X_OUT;
	wire GND_BIT;

	wire CLKFX_BUF;
	wire CLK13MHZ;
	reg  CLK6M5MK;

	assign GND_BIT = 0;
	assign CLKFB_IN = CLK2X_OUT;
	assign CLK32IB = CLKIN_IBUFG;

	assign CLKFX_BUF = CLK6M5MK;

	IBUFG  CLKIN_IBUFG_INST (.I(CLKIN),     .O(CLKIN_IBUFG));
	BUFG   CLK2X_BUFG_INST  (.I(CLK2X_BUF), .O(CLK2X_OUT));
	BUFG   CLKFX_BUFG_INST  (.I(CLKFX_BUF), .O(CLK6M5));

	DCM_SP #(
				.CLK_FEEDBACK("2X"),
				.CLKDV_DIVIDE(2.0),
				.CLKFX_DIVIDE(32),
				.CLKFX_MULTIPLY(13),
				.CLKIN_DIVIDE_BY_2("FALSE"),
				.CLKIN_PERIOD(31.250),
				.CLKOUT_PHASE_SHIFT("NONE"),
				.DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"),
				.DFS_FREQUENCY_MODE("LOW"),
				.DLL_FREQUENCY_MODE("LOW"),
				.DUTY_CYCLE_CORRECTION("TRUE"),
				.FACTORY_JF(16'hC080), 
				.PHASE_SHIFT(0),
				.STARTUP_WAIT("FALSE") 
			) 
	DCM_SP_PRI (
					.CLKFB(CLKFB_IN),
					.CLKIN(CLKIN_IBUFG),
					.DSSEN(GND_BIT),
					.PSCLK(GND_BIT),
					.PSEN(GND_BIT),     
					.PSINCDEC(GND_BIT),
					.RST(RST_IN),
					.CLKDV(CLK16M),
					.CLKFX(CLK13MHZ),
					.CLKFX180(),
					.CLK0(),	.CLK90(), 			.CLK180(), 				.CLK270(), 
					.CLK2X(CLK2X_BUF),	.CLK2X180(), 
					.LOCKED(LOCKED), 
					.PSDONE(),	
					.STATUS()
				);



	initial CLK6M5MK <= 0;

	always @(posedge CLK13MHZ) begin
		CLK6M5MK <= !CLK6M5MK;
	end

endmodule
