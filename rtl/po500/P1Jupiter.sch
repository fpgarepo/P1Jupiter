<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <attr value="PartialBusOnly|BaseDashIndex" name="RenameBusIO" />
    <netlist>
        <signal name="clk_ace" />
        <signal name="PS2A_CLK" />
        <signal name="clk_vga" />
        <signal name="rst_n" />
        <signal name="data(7:0)" />
        <signal name="RAMCS_n" />
        <signal name="WE_n" />
        <signal name="addr(15:0)" />
        <signal name="O_HSYNC" />
        <signal name="O_VSYNC" />
        <signal name="O_VIDEO_B(3:0)" />
        <signal name="O_VIDEO_G(3:0)" />
        <signal name="O_VIDEO_R(3:0)" />
        <signal name="O_LED(3:0)" />
        <signal name="O_LED(2)" />
        <signal name="PS2A_DATA" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="tape_in" />
        <signal name="O_LED(3)" />
        <signal name="O_LED(0)" />
        <signal name="O_LED(1)" />
        <signal name="CLK32M" />
        <signal name="pll_lock" />
        <signal name="BTN_RESET" />
        <signal name="AUDIO_R" />
        <signal name="AUDIO_L" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="BTN_UP" />
        <signal name="clk_high" />
        <signal name="XLXN_53" />
        <port polarity="Input" name="PS2A_CLK" />
        <port polarity="Output" name="O_HSYNC" />
        <port polarity="Output" name="O_VSYNC" />
        <port polarity="Output" name="O_VIDEO_B(3:0)" />
        <port polarity="Output" name="O_VIDEO_G(3:0)" />
        <port polarity="Output" name="O_VIDEO_R(3:0)" />
        <port polarity="Output" name="O_LED(3:0)" />
        <port polarity="Input" name="PS2A_DATA" />
        <port polarity="Input" name="CLK32M" />
        <port polarity="Input" name="BTN_RESET" />
        <port polarity="BiDirectional" name="AUDIO_R" />
        <port polarity="BiDirectional" name="AUDIO_L" />
        <port polarity="Input" name="BTN_UP" />
        <blockdef name="ace">
            <timestamp>2012-3-13T21:1:53</timestamp>
            <rect width="256" x="64" y="-832" height="832" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <line x2="0" y1="-656" y2="-656" x1="64" />
            <line x2="0" y1="-512" y2="-512" x1="64" />
            <line x2="0" y1="-368" y2="-368" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="384" y1="-800" y2="-800" x1="320" />
            <line x2="384" y1="-736" y2="-736" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="p1clock">
            <timestamp>2012-3-14T20:6:47</timestamp>
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="384" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="p1ram">
            <timestamp>2024-8-12T20:48:20</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="ace2vga">
            <timestamp>2012-3-13T20:59:0</timestamp>
            <rect width="64" x="320" y="84" height="24" />
            <line x2="384" y1="96" y2="96" x1="320" />
            <rect width="64" x="320" y="148" height="24" />
            <line x2="384" y1="160" y2="160" x1="320" />
            <rect width="64" x="320" y="212" height="24" />
            <line x2="384" y1="224" y2="224" x1="320" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-256" y2="-256" x1="320" />
            <rect width="256" x="64" y="-512" height="768" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="soundselect">
            <timestamp>2020-5-3T21:15:10</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-192" height="256" />
        </blockdef>
        <blockdef name="debounce">
            <timestamp>2020-5-3T21:52:23</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="p1ramext">
            <timestamp>2024-8-12T20:48:28</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <block symbolname="ace" name="zxace">
            <blockpin signalname="rst_n" name="Rst_n" />
            <blockpin signalname="clk_ace" name="Clk" />
            <blockpin signalname="XLXN_9" name="PS2_Clk" />
            <blockpin signalname="XLXN_8" name="PS2_Data" />
            <blockpin signalname="tape_in" name="Tape_In" />
            <blockpin signalname="data(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_38" name="Tape_Out" />
            <blockpin signalname="XLXN_39" name="Sound" />
            <blockpin name="CVBS" />
            <blockpin name="OE_n" />
            <blockpin signalname="WE_n" name="WE_n" />
            <blockpin signalname="RAMCS_n" name="RAMCS_n" />
            <blockpin name="ROMCS_n" />
            <blockpin name="PGM_n" />
            <blockpin signalname="addr(15:0)" name="A(15:0)" />
        </block>
        <block symbolname="ace2vga" name="u_vga">
            <blockpin signalname="rst_n" name="Rst_n" />
            <blockpin signalname="clk_ace" name="ace_clk" />
            <blockpin signalname="clk_vga" name="vga_clk" />
            <blockpin signalname="WE_n" name="WE_n" />
            <blockpin signalname="addr(15:0)" name="Addr(15:0)" />
            <blockpin signalname="data(7:0)" name="DI(7:0)" />
            <blockpin signalname="O_HSYNC" name="hsync" />
            <blockpin signalname="O_VSYNC" name="vsync" />
            <blockpin signalname="O_VIDEO_R(3:0)" name="video_r(3:0)" />
            <blockpin signalname="O_VIDEO_G(3:0)" name="video_g(3:0)" />
            <blockpin signalname="O_VIDEO_B(3:0)" name="video_b(3:0)" />
        </block>
        <block symbolname="inv" name="XLXI_15">
            <blockpin signalname="XLXN_8" name="I" />
            <blockpin signalname="O_LED(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_17">
            <blockpin signalname="PS2A_DATA" name="I" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_18">
            <blockpin signalname="PS2A_CLK" name="I" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_19">
            <blockpin signalname="tape_in" name="I" />
            <blockpin signalname="O_LED(3)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="rst_n" name="I" />
            <blockpin signalname="O_LED(0)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_30">
            <blockpin signalname="pll_lock" name="I" />
            <blockpin signalname="O_LED(1)" name="O" />
        </block>
        <block symbolname="soundselect" name="u_aud_sel">
            <blockpin signalname="BTN_UP" name="btn_in" />
            <blockpin signalname="XLXN_38" name="tape_out" />
            <blockpin signalname="XLXN_39" name="sound_out" />
            <blockpin signalname="AUDIO_L" name="aud_l" />
            <blockpin signalname="AUDIO_R" name="aud_r" />
            <blockpin signalname="tape_in" name="tape_in" />
            <blockpin signalname="clk_ace" name="clk" />
        </block>
        <block symbolname="p1clock" name="u_clk">
            <blockpin signalname="CLK32M" name="CLKIN" />
            <blockpin signalname="XLXN_53" name="RST_IN" />
            <blockpin signalname="clk_ace" name="CLK6M5" />
            <blockpin signalname="pll_lock" name="LOCKED" />
            <blockpin signalname="clk_high" name="CLK32IB" />
            <blockpin signalname="clk_vga" name="CLK16M" />
        </block>
        <block symbolname="debounce" name="u_deb_rst">
            <blockpin signalname="clk_high" name="clk" />
            <blockpin signalname="BTN_RESET" name="btn" />
            <blockpin signalname="XLXN_53" name="sig" />
        </block>
        <block symbolname="inv" name="XLXI_32">
            <blockpin signalname="XLXN_53" name="I" />
            <blockpin signalname="rst_n" name="O" />
        </block>
        <block symbolname="p1ram" name="u_intram">
            <blockpin signalname="clk_ace" name="Clk" />
            <blockpin signalname="RAMCS_n" name="CE_n" />
            <blockpin signalname="WE_n" name="WE_n" />
            <blockpin signalname="addr(15:0)" name="A(15:0)" />
            <blockpin signalname="data(7:0)" name="D(7:0)" />
        </block>
        <block symbolname="p1ramext" name="u_extram">
            <blockpin signalname="clk_ace" name="Clk" />
            <blockpin signalname="RAMCS_n" name="CE_n" />
            <blockpin signalname="WE_n" name="WE_n" />
            <blockpin signalname="addr(15:0)" name="A(15:0)" />
            <blockpin signalname="data(7:0)" name="D(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <iomarker fontsize="28" x="448" y="1168" name="PS2A_CLK" orien="R180" />
        <iomarker fontsize="28" x="448" y="1312" name="PS2A_DATA" orien="R180" />
        <branch name="rst_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="880" type="branch" />
            <wire x2="1520" y1="880" y2="880" x1="1440" />
        </branch>
        <branch name="clk_ace">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="1024" type="branch" />
            <wire x2="1520" y1="1024" y2="1024" x1="1440" />
        </branch>
        <instance x="1520" y="1680" name="zxace" orien="R0">
        </instance>
        <branch name="addr(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="1584" type="branch" />
            <wire x2="2000" y1="1584" y2="1584" x1="1904" />
        </branch>
        <branch name="data(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="1648" type="branch" />
            <wire x2="2000" y1="1648" y2="1648" x1="1904" />
        </branch>
        <branch name="RAMCS_n">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="1392" type="branch" />
            <wire x2="2000" y1="1392" y2="1392" x1="1904" />
        </branch>
        <branch name="WE_n">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2000" y="1328" type="branch" />
            <wire x2="2000" y1="1328" y2="1328" x1="1904" />
        </branch>
        <instance x="2416" y="1984" name="u_vga" orien="R0">
        </instance>
        <branch name="rst_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1504" type="branch" />
            <wire x2="2416" y1="1504" y2="1504" x1="2288" />
        </branch>
        <branch name="clk_ace">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1568" type="branch" />
            <wire x2="2416" y1="1568" y2="1568" x1="2288" />
        </branch>
        <branch name="clk_vga">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1632" type="branch" />
            <wire x2="2416" y1="1632" y2="1632" x1="2288" />
        </branch>
        <branch name="addr(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1888" type="branch" />
            <wire x2="2416" y1="1888" y2="1888" x1="2288" />
        </branch>
        <branch name="data(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="1952" type="branch" />
            <wire x2="2416" y1="1952" y2="1952" x1="2288" />
        </branch>
        <branch name="WE_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2288" y="2016" type="branch" />
            <wire x2="2416" y1="2016" y2="2016" x1="2288" />
        </branch>
        <branch name="O_HSYNC">
            <wire x2="2928" y1="1504" y2="1504" x1="2800" />
        </branch>
        <branch name="O_VSYNC">
            <wire x2="2928" y1="1728" y2="1728" x1="2800" />
        </branch>
        <branch name="O_VIDEO_B(3:0)">
            <wire x2="2928" y1="2208" y2="2208" x1="2800" />
        </branch>
        <branch name="O_VIDEO_G(3:0)">
            <wire x2="2928" y1="2144" y2="2144" x1="2800" />
        </branch>
        <branch name="O_VIDEO_R(3:0)">
            <wire x2="2928" y1="2080" y2="2080" x1="2800" />
        </branch>
        <iomarker fontsize="28" x="2928" y="1504" name="O_HSYNC" orien="R0" />
        <iomarker fontsize="28" x="2928" y="1728" name="O_VSYNC" orien="R0" />
        <iomarker fontsize="28" x="2928" y="2080" name="O_VIDEO_R(3:0)" orien="R0" />
        <iomarker fontsize="28" x="2928" y="2144" name="O_VIDEO_G(3:0)" orien="R0" />
        <iomarker fontsize="28" x="2928" y="2208" name="O_VIDEO_B(3:0)" orien="R0" />
        <branch name="O_LED(3:0)">
            <wire x2="2928" y1="672" y2="672" x1="2832" />
        </branch>
        <iomarker fontsize="28" x="2928" y="672" name="O_LED(3:0)" orien="R0" />
        <branch name="O_LED(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="1920" type="branch" />
            <wire x2="1904" y1="1920" y2="1920" x1="1840" />
        </branch>
        <instance x="1616" y="1952" name="XLXI_15" orien="R0" />
        <branch name="PS2A_DATA">
            <wire x2="544" y1="1312" y2="1312" x1="448" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1280" y1="1312" y2="1312" x1="768" />
            <wire x2="1280" y1="1312" y2="1920" x1="1280" />
            <wire x2="1616" y1="1920" y2="1920" x1="1280" />
            <wire x2="1520" y1="1312" y2="1312" x1="1280" />
        </branch>
        <branch name="PS2A_CLK">
            <wire x2="544" y1="1168" y2="1168" x1="448" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1520" y1="1168" y2="1168" x1="768" />
        </branch>
        <instance x="1616" y="1840" name="XLXI_19" orien="R0" />
        <branch name="O_LED(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="1808" type="branch" />
            <wire x2="1904" y1="1808" y2="1808" x1="1840" />
        </branch>
        <branch name="O_LED(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="2144" type="branch" />
            <wire x2="1904" y1="2144" y2="2144" x1="1840" />
        </branch>
        <branch name="rst_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1472" y="2144" type="branch" />
            <wire x2="1616" y1="2144" y2="2144" x1="1472" />
        </branch>
        <instance x="1616" y="2176" name="XLXI_8" orien="R0" />
        <branch name="O_LED(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1904" y="2032" type="branch" />
            <wire x2="1904" y1="2032" y2="2032" x1="1840" />
        </branch>
        <branch name="pll_lock">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="1472" y="2032" type="branch" />
            <wire x2="1616" y1="2032" y2="2032" x1="1472" />
        </branch>
        <instance x="1616" y="2064" name="XLXI_30" orien="R0" />
        <branch name="BTN_RESET">
            <wire x2="688" y1="800" y2="800" x1="448" />
        </branch>
        <branch name="tape_in">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="768" y="1456" type="branch" />
            <wire x2="1360" y1="1456" y2="1456" x1="768" />
            <wire x2="1360" y1="1456" y2="1808" x1="1360" />
            <wire x2="1616" y1="1808" y2="1808" x1="1360" />
            <wire x2="1520" y1="1456" y2="1456" x1="1360" />
        </branch>
        <instance x="544" y="1344" name="XLXI_17" orien="R0" />
        <instance x="544" y="1200" name="XLXI_18" orien="R0" />
        <branch name="AUDIO_R">
            <wire x2="2928" y1="944" y2="944" x1="2800" />
        </branch>
        <branch name="AUDIO_L">
            <wire x2="2928" y1="880" y2="880" x1="2800" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="2416" y1="880" y2="880" x1="1904" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="2416" y1="944" y2="944" x1="1904" />
        </branch>
        <iomarker fontsize="28" x="2928" y="880" name="AUDIO_L" orien="R0" />
        <iomarker fontsize="28" x="2928" y="944" name="AUDIO_R" orien="R0" />
        <branch name="tape_in">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2928" y="816" type="branch" />
            <wire x2="2928" y1="816" y2="816" x1="2800" />
        </branch>
        <branch name="BTN_UP">
            <wire x2="2416" y1="816" y2="816" x1="2304" />
        </branch>
        <branch name="clk_ace">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2304" y="1008" type="branch" />
            <wire x2="2416" y1="1008" y2="1008" x1="2304" />
        </branch>
        <instance x="2416" y="976" name="u_aud_sel" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2304" y="816" name="BTN_UP" orien="R180" />
        <branch name="clk_ace">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="288" type="branch" />
            <wire x2="1920" y1="288" y2="288" x1="1904" />
        </branch>
        <branch name="clk_vga">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="608" type="branch" />
            <wire x2="1920" y1="608" y2="608" x1="1904" />
        </branch>
        <branch name="clk_high">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="480" type="branch" />
            <wire x2="1920" y1="480" y2="480" x1="1904" />
        </branch>
        <branch name="pll_lock">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1920" y="416" type="branch" />
            <wire x2="1920" y1="416" y2="416" x1="1904" />
        </branch>
        <instance x="1520" y="512" name="u_clk" orien="R0">
        </instance>
        <branch name="rst_n">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1424" y="736" type="branch" />
            <wire x2="1424" y1="736" y2="736" x1="1360" />
        </branch>
        <instance x="1136" y="768" name="XLXI_32" orien="R0" />
        <instance x="688" y="832" name="u_deb_rst" orien="R0">
        </instance>
        <iomarker fontsize="28" x="448" y="800" name="BTN_RESET" orien="R180" />
        <iomarker fontsize="28" x="432" y="288" name="CLK32M" orien="R180" />
        <branch name="CLK32M">
            <wire x2="1520" y1="288" y2="288" x1="432" />
        </branch>
        <branch name="clk_high">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="592" y="736" type="branch" />
            <wire x2="688" y1="736" y2="736" x1="592" />
        </branch>
        <branch name="XLXN_53">
            <wire x2="1088" y1="736" y2="736" x1="1072" />
            <wire x2="1136" y1="736" y2="736" x1="1088" />
            <wire x2="1520" y1="480" y2="480" x1="1088" />
            <wire x2="1088" y1="480" y2="736" x1="1088" />
        </branch>
        <branch name="RAMCS_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2400" type="branch" />
            <wire x2="432" y1="2400" y2="2400" x1="416" />
            <wire x2="528" y1="2400" y2="2400" x1="432" />
        </branch>
        <branch name="WE_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2464" type="branch" />
            <wire x2="432" y1="2464" y2="2464" x1="416" />
            <wire x2="528" y1="2464" y2="2464" x1="432" />
        </branch>
        <branch name="data(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="2336" type="branch" />
            <wire x2="976" y1="2336" y2="2336" x1="912" />
            <wire x2="992" y1="2336" y2="2336" x1="976" />
        </branch>
        <branch name="addr(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2528" type="branch" />
            <wire x2="432" y1="2528" y2="2528" x1="416" />
            <wire x2="528" y1="2528" y2="2528" x1="432" />
        </branch>
        <branch name="clk_ace">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2336" type="branch" />
            <wire x2="432" y1="2336" y2="2336" x1="416" />
            <wire x2="528" y1="2336" y2="2336" x1="432" />
        </branch>
        <branch name="data(7:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="992" y="1952" type="branch" />
            <wire x2="976" y1="1952" y2="1952" x1="912" />
            <wire x2="992" y1="1952" y2="1952" x1="976" />
        </branch>
        <branch name="clk_ace">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="1952" type="branch" />
            <wire x2="432" y1="1952" y2="1952" x1="416" />
            <wire x2="528" y1="1952" y2="1952" x1="432" />
        </branch>
        <branch name="RAMCS_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2016" type="branch" />
            <wire x2="432" y1="2016" y2="2016" x1="416" />
            <wire x2="528" y1="2016" y2="2016" x1="432" />
        </branch>
        <branch name="WE_n">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2080" type="branch" />
            <wire x2="432" y1="2080" y2="2080" x1="416" />
            <wire x2="528" y1="2080" y2="2080" x1="432" />
        </branch>
        <branch name="addr(15:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="416" y="2144" type="branch" />
            <wire x2="432" y1="2144" y2="2144" x1="416" />
            <wire x2="528" y1="2144" y2="2144" x1="432" />
        </branch>
        <instance x="528" y="2176" name="u_intram" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="192" y="-32" type="instance" />
        </instance>
        <instance x="528" y="2560" name="u_extram" orien="R0">
            <attrtext style="fontsize:28;fontname:Arial" attrname="InstName" x="192" y="-32" type="instance" />
            <attrtext style="fontsize:28;fontname:Arial;displayformat:NAMEEQUALSVALUE" attrname="SymbolName" x="0" y="0" type="instance" />
        </instance>
    </sheet>
</drawing>