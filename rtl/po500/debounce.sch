<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clk" />
        <signal name="btn" />
        <signal name="XLXN_72" />
        <signal name="XLXN_83" />
        <signal name="XLXN_84" />
        <signal name="XLXN_85" />
        <signal name="XLXN_86" />
        <signal name="XLXN_87" />
        <signal name="XLXN_88" />
        <signal name="XLXN_89" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_92" />
        <signal name="XLXN_93" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="XLXN_96" />
        <signal name="XLXN_97" />
        <signal name="XLXN_98" />
        <signal name="XLXN_99" />
        <signal name="XLXN_102" />
        <signal name="XLXN_105" />
        <signal name="XLXN_106" />
        <signal name="debounce(15)" />
        <signal name="debounce(15:0)" />
        <signal name="sig" />
        <signal name="XLXN_101" />
        <signal name="XLXN_100" />
        <signal name="XLXN_114" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="btn" />
        <port polarity="Output" name="sig" />
        <blockdef name="fdrs">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-352" y2="-352" x1="0" />
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="192" y1="-320" y2="-352" x1="192" />
            <line x2="64" y1="-352" y2="-352" x1="192" />
        </blockdef>
        <blockdef name="cb16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <rect width="64" x="320" y="-268" height="24" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="fdrs" name="XLXI_25">
            <blockpin signalname="debounce(15)" name="C" />
            <blockpin signalname="btn" name="D" />
            <blockpin name="R" />
            <blockpin name="S" />
            <blockpin signalname="sig" name="Q" />
        </block>
        <block symbolname="xor2" name="XLXI_38">
            <blockpin signalname="sig" name="I0" />
            <blockpin signalname="btn" name="I1" />
            <blockpin signalname="XLXN_101" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_39">
            <blockpin signalname="XLXN_101" name="I" />
            <blockpin signalname="XLXN_100" name="O" />
        </block>
        <block symbolname="cb16ce" name="XLXI_13">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_101" name="CE" />
            <blockpin signalname="XLXN_100" name="CLR" />
            <blockpin name="CEO" />
            <blockpin signalname="debounce(15:0)" name="Q(15:0)" />
            <blockpin name="TC" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="clk">
            <wire x2="528" y1="928" y2="928" x1="512" />
            <wire x2="2080" y1="928" y2="928" x1="528" />
        </branch>
        <branch name="btn">
            <wire x2="784" y1="576" y2="576" x1="512" />
            <wire x2="896" y1="576" y2="576" x1="784" />
            <wire x2="784" y1="576" y2="896" x1="784" />
            <wire x2="1392" y1="896" y2="896" x1="784" />
        </branch>
        <iomarker fontsize="28" x="512" y="928" name="clk" orien="R180" />
        <branch name="debounce(15)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="656" y="704" type="branch" />
            <wire x2="672" y1="704" y2="704" x1="656" />
            <wire x2="896" y1="704" y2="704" x1="672" />
        </branch>
        <iomarker fontsize="28" x="512" y="576" name="btn" orien="R180" />
        <branch name="debounce(15:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2624" y="800" type="branch" />
            <wire x2="2624" y1="800" y2="800" x1="2464" />
        </branch>
        <instance x="896" y="832" name="XLXI_25" orien="R0" />
        <branch name="sig">
            <wire x2="1312" y1="576" y2="576" x1="1280" />
            <wire x2="1312" y1="576" y2="832" x1="1312" />
            <wire x2="1392" y1="832" y2="832" x1="1312" />
            <wire x2="2672" y1="576" y2="576" x1="1312" />
        </branch>
        <instance x="1392" y="768" name="XLXI_38" orien="M180" />
        <instance x="1792" y="1056" name="XLXI_39" orien="R0" />
        <branch name="XLXN_101">
            <wire x2="1664" y1="864" y2="864" x1="1648" />
            <wire x2="1664" y1="864" y2="1024" x1="1664" />
            <wire x2="1792" y1="1024" y2="1024" x1="1664" />
            <wire x2="2080" y1="864" y2="864" x1="1664" />
        </branch>
        <branch name="XLXN_100">
            <wire x2="2080" y1="1024" y2="1024" x1="2016" />
        </branch>
        <instance x="2080" y="1056" name="XLXI_13" orien="R0" />
        <iomarker fontsize="28" x="2672" y="576" name="sig" orien="R0" />
    </sheet>
</drawing>