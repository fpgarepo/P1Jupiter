`timescale 1ns/100ps
module p1ramext # ( parameter AddrWidth = 16, RAMSize = 14, RAMSel = 4'b01, DataWidth = 8 )
(
	input Clk, 
	input CE_n, 
	input WE_n,
	input [AddrWidth-1:0] A,
	inout [DataWidth-1:0] D
);

	reg 	[DataWidth-1:0] RAM [0:2**(RAMSize)-1];
	reg 	[RAMSize-1:0]   A_r;
	wire	En;

	// External RAM should be fully decoded, starts at 0x4000
	// Only enable if the high bits are matching the selector
	assign En = !CE_n && (A[AddrWidth-1:RAMSize] == RAMSel);

	always @(posedge Clk) begin
		A_r <= A[RAMSize-1:0];
		if (En) begin
			if (!WE_n) RAM[A_r] <= D;
		end
	end

	assign D = (En && WE_n) ? RAM[A_r] : {DataWidth{1'bz}};

endmodule
