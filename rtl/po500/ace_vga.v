// VGA 800x600
`timescale 1ns / 100ps

module dualport_ram #(
	parameter ADDR_WIDTH = 10, 
	parameter DATA_WIDTH = 8
	) (
	input wire a_clk, 
	input wire a_wr, 
	input wire [ADDR_WIDTH-1:0] a_addr, 
	input wire [DATA_WIDTH-1:0] a_din, 
	output reg [DATA_WIDTH-1:0] a_dout,
	input wire b_clk, 
	input wire b_wr, 
	input wire [ADDR_WIDTH-1:0] b_addr, 
	input wire [DATA_WIDTH-1:0] b_din, 
	output reg [DATA_WIDTH-1:0] b_dout
	);
	reg [DATA_WIDTH-1:0] mem [(2**ADDR_WIDTH)-1:0];
	always @(posedge a_clk) begin
		a_dout <= mem[a_addr];
		if (a_wr) begin
			a_dout 		<= a_din;
			mem[a_addr] <= a_din;
		end
	end
	always @(posedge b_clk) begin
		b_dout <= mem[b_addr];
		if (b_wr) begin
			b_dout 		<= b_din;
			mem[b_addr] <= b_din;
		end
	end
endmodule


///VGA module//////////////////////////////////////////////////////////////////
module ace_vga(
    input Rst_n,
    input ace_clk,
	 input vga_clk,
    input CSV_n,
    input CSC_n,
    input Wr_n,
    input [9:0] Addr,
    input [7:0] DI,
    output hsync,
    output vsync,
    output video
    );
	
///VGA generator///////////////////////////////////////////////////////////////

	reg [10:0] cnt_hor;
	reg  [9:0] cnt_ver;
	wire viden;
	wire curpix;

	// VGA params - active / front_porch / sync / back_porch
	// horizontal -    800 /          40 /  128 /         88
	//   vertical -    600 /           1 /    4 /         23
	//  frequency - 40MHz pix / 37.88kHz hor / 60.32Hz ver
	
	always @(posedge vga_clk) begin
		if (!Rst_n || cnt_hor == 1055) cnt_hor <= 0; 
		else cnt_hor <= cnt_hor + 1;
	end
	
	always @(posedge vga_clk) begin
		if (!Rst_n || (cnt_ver >= 627 && cnt_hor >= 967)) cnt_ver <= 0; 
		else if (cnt_hor == 967) cnt_ver <= cnt_ver + 1;
	end

	assign hsync = (cnt_hor >= 840 && cnt_hor <= 967) ? 1'b0 : 1'b1;
	assign vsync = (cnt_ver >= 602 && cnt_ver <= 605) ? 1'b0 : 1'b1;

	// video test
	//assign viden = (cnt_hor < 800 && cnt_ver < 600) ? 1'b1 : 1'b0; 
   //assign curpix = cnt_ver[3] ^ cnt_hor[3];

   // VGA active
	assign viden = (cnt_hor < 528 && cnt_ver < 384) ? 1'b1 : 1'b0; 
	assign video = viden & curpix;


///Video and Character RAM shadow (CPU write-only)/////////////////////////////
	
   wire [7:0] vga_char;
   wire [7:0] vga_cpix;
	wire [9:0] VRAM_addr; //@todo reduce to 768 bytes
	wire [9:0] CRAM_addr;
	wire VRAM_wren;
	wire CRAM_wren;
	
	assign VRAM_wren = (!Wr_n && !CSV_n);
	assign CRAM_wren = (!Wr_n && !CSC_n);
	assign VRAM_addr = { cnt_ver[8:4],  cnt_hor[8:4] };
	assign CRAM_addr = { vga_char[6:0], cnt_ver[3:1] };

	dualport_ram VidRAM (.a_clk(ace_clk),.a_wr(VRAM_wren),.a_addr(Addr),     .a_din(DI),.a_dout(),
	                     .b_clk(vga_clk),.b_wr(1'b0),     .b_addr(VRAM_addr),.b_din(),  .b_dout(vga_char));
	dualport_ram ChrRAM (.a_clk(ace_clk),.a_wr(CRAM_wren),.a_addr(Addr),     .a_din(DI),.a_dout(),
	                     .b_clk(vga_clk),.b_wr(1'b0),     .b_addr(CRAM_addr),.b_din(),  .b_dout(vga_cpix));

   //assign curpix = vga_cpix[7 - cnt_hor[3:1]] ^ vga_char[7];
	
	reg [7:0] buf_cpix;
	reg [2:0] cnt_cpix;
	reg buf_cinv;
	
	always @(posedge vga_clk) begin
	    if (!Rst_n || !viden) begin
			  cnt_cpix = 0;
			  buf_cpix <= 8'h00;
			  buf_cinv <= 0;
		 end else 
		 if (cnt_hor[0]) begin
		     if (cnt_cpix == 7) begin 
			      buf_cpix <= vga_cpix;
					buf_cinv <= vga_char[7];
			  end else begin
			      buf_cpix[7:1] <= buf_cpix[6:0];
			  end
	        cnt_cpix = cnt_cpix + 1;
		 end
	end
	
	assign curpix = buf_cpix[7] ^ buf_cinv;
	
	
endmodule




